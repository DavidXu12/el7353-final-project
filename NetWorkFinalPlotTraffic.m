% generate 1s aggregation first
s1=zeros(10010,1);
for i=1:length(Time)
    segn=floor(Time(i))+1;
    s1(segn)=s1(segn)+Length(i);
end
outs1=zeros(100,1); %sample points for better plot
for i=1:100
    outs1(i)=s1(1+(i-1)*34);
end 
plot(0:34:3367,outs1,'-square');
axis([0 3340 0 max(outs1)*1.35])
legend('1s aggregation (expo)')
ylabel('throughput (bytes)');xlabel('time (sec)');


% generate 10s aggregation

s10=zeros(1001,1);
for i=1:length(Time)
    segn=floor(Time(i)/10)+1;
    s10(segn)=s10(segn)+Length(i);
end
outs10=zeros(100,1);
for i=1:100
    outs10(i)=s10(10+(i-1)*4);
end 
plot(0:40:3961,outs10,'-square');
axis([0 4000 0 max(outs10)*1.35])
legend('10s aggregation (expo)')
ylabel('throughput (bytes)');xlabel('time (sec)');

%generate 100s aggregation
s100=zeros(101,1);
for i=1:length(Time)
    segn=floor(Time(i)/100)+1;
    s100(segn)=s100(segn)+Length(i);
end
plot(0:100:9900,s100(1:100,1),'-square');
axis([0 10000 0 max(s100)*1.5])
legend('100s aggregation (expo)')
ylabel('throughput (bytes)');xlabel('time (sec)');

%mearge them 
a1=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.05a100s.tif'));
a2=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.35a100s.tif'));
a3=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.95a100s.tif'));
a4=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/expo100s.tif'));
b1=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.05a10s.tif'));
b2=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.35a10s.tif'));
b3=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.95a10s.tif'));
b4=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/expo10s.tif'));
c1=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.05a1s.tif'));
c2=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.35a1s.tif'));
c3=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/1.95a1s.tif'));
c4=im2double(imread('/Users/ShengheXu/Desktop/Network modeling/final/plots/expo1s.tif'));
[l,m,k]=size(a1);
merg=zeros(l*3,m*4,k);
merg(1:l,1:m,:)=a1;
merg(1:l,m+1:2*m,:)=a2;
merg(1:l,2*m+1:3*m,:)=a3;
merg(1:l,3*m+1:4*m,:)=a4;
merg(l+1:2*l,1:m,:)=b1;
merg(1+l:2*l,m+1:2*m,:)=b2;
merg(1+l:2*l,2*m+1:3*m,:)=b3;
merg(1+l:2*l,3*m+1:4*m,:)=b4;
merg(2*l+1:3*l,1:m,:)=c1;
merg(1+2*l:3*l,m+1:2*m,:)=c2;
merg(1+2*l:3*l,2*m+1:3*m,:)=c3;
merg(1+2*l:3*l,3*m+1:4*m,:)=c4;


