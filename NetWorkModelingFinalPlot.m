%create a 2d matrix for data. alpha=1.05 1.35 1.65 1.95
%buffer 2 3 6 10 28 46 64 128

queues=[582.481,1853.830,4571.470,8286.074,26045.280,44412.220,62015.160,124981.200;
        328.420,803.9500,2720.738,5585.550,15011.360,19703.900,26546.460,38250.560;
        8.63000,44.04500,157.410,281.0230,614.7860,912.17300,594.91800,987.26900;
        0.42400,8.276000,24.51000,76.47600,64.37900,40.806000,114.67100,109.99500];
loss=[0.264,0.189,0.170,0.159,0.152,0.147,0.145,0.123;
      0.108,0.107,0.100,0.087,0.052,0.021,0.024,0.010;
      0.042,0.010,0.010,0.006,0.001,0.000,0.000,0.000;
      0.020,0.004,0.001,0.001,0.000,0.000,0.000,0.000];
  loss=loss*100;
  
  %generate 1st plot
  as=[1.05 1.35 1.65 1.95];
  plot(as,loss(:,1),'-o',as,loss(:,2),'-*',as,loss(:,3),'-+',as,loss(:,4),'-square',as,loss(:,5),'-o',as,loss(:,6),'-*',as,loss(:,7),'-+',as,loss(:,8),'-square');
  legend('link buffer 2 kB','link buffer 3 kB','link buffer 6 kB','link buffer 10 kB','link buffer 28 kB','link buffer 46 kB','link buffer 64 kB','link buffer 128 kB');
  xlabel('alpha');ylabel('packet loss rate (percent)');
  % generate 2nd plot
  plot(as,queues(:,1),'-o',as,queues(:,2),'-*',as,queues(:,3),'-+',as,queues(:,4),'-square',as,queues(:,5),'-o',as,queues(:,6),'-*',as,queues(:,7),'-+',as,queues(:,8),'-square');
  legend('link buffer 2 kB','link buffer 3 kB','link buffer 6 kB','link buffer 10 kB','link buffer 28 kB','link buffer 46 kB','link buffer 64 kB','link buffer 128 kB');
  xlabel('alpha');ylabel('mean queue length (byte)');
  % generate 3rd plot
  plot(loss(1,:),queues(1,:),'-o',loss(2,:),queues(2,:),'-*',loss(3,:),queues(3,:),'-square',loss(4,:),queues(4,:),'-+');
  legend('alpha 1.05','alpha 1.35','alpha 1.65','alpha 1.95');
  xlabel('packet loss rate (percent)');ylabel('mean queue length (byte)');
   