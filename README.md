# Introduction
Measurements of network traffic have shown that real traffic varies at a great range of scales. We have reproduced several experiments on the relationship between file size distribution and network performance from the paper 'Park, Kihong, Gitae Kim, and Mark Crovella. "On the relationship between file sizes, transport protocols, and self-similar network traffic." Network Protocols, 1996. Proceedings., 1996 International Conference on. IEEE, 1996.' It may take 18 hours to run all the experiments and reproduce our figure.


# Background
Measurements of network traffic have shown that network traffic exhibits variability at a large scale. The assumption of exponential distributed packet size may not be applicable to all cases. In this experiment we consider the impact of heavy-tailed distribution of file sizes on network performance. One of the simplest heavy-tailed distribution is Pareto distribution. The parameter k of Preto distribution represents the smallest possible value of the random variable. Alpha is related to the heavy-tailedness of distribution.

#Results

We use a topology consisting of 16 clients, 2 routers and 2 servers . 

![topology.png](https://bitbucket.org/repo/gB4R4z/images/4218315529-topology.png)

Clients send request to servers for files. We first measure the throughput of the bottleneck link between the 2 routers .

 ![repodd1.png](https://bitbucket.org/repo/gB4R4z/images/2895823203-repodd1.png)
Then we change the buffer size of the bottleneck link queue and compare the packet loss rate and buffer occupancy introduced by different file size distributions.
 ![repodd2.png](https://bitbucket.org/repo/gB4R4z/images/1071245412-repodd2.png)

 Most of our results are quite similar to the ones obtained by simulation in the original paper. This means that heavy tail distribution of file sizes may introduce great self-similarity in network traffic.
Below is the result of our replication of the throughput measurement part. 

![rsl1.png](https://bitbucket.org/repo/gB4R4z/images/3227356115-rsl1.png)

We can see that this figure is quite similar to the figure we are trying to reproduce. However, the magnitudes of the plots are lower those of the original figure. This is most likely to be caused by the selection of different value of k for Pareto distribution. We can certainly achieve similar magnitudes by trying different values of k. But this may take a lot of time. And as we are mostly interested in the trend of the figure instead of its magnitude, it is hardly necessary for us to achieve the same magnitude.  

Following are replications of the three parts of figure 9 from the original paper. 

![rsl2.1.png](https://bitbucket.org/repo/gB4R4z/images/2135415317-rsl2.1.png)
![rsl2.2.png](https://bitbucket.org/repo/gB4R4z/images/3274821883-rsl2.2.png)
![rsl2.3.png](https://bitbucket.org/repo/gB4R4z/images/964180655-rsl2.3.png)

From figure 5.1 we can see that our packet loss rate drops much faster when traffic self-similarity drops than the original figure. This may have been caused by our different selection of value k for Pareto distribution and bandwidth. The original paper may have selected a smaller bandwidth so that even the bursts of traffic are smaller there would still be considerable loss of packets. Figure 5.2 is quite similar to the original plot. The difference between figure 5.3 and the original figure may have been caused by selection of different parameters. Or this may indicate that self-similarity of network traffic have much more impact than that shown in the original paper.


# Run my experiment
## Experiment overview
According to the original paper, most of the experiments were performed on the similar topology as the one in figure 1 but with 32 clients. But the author also mentioned that similar results can be achieved by changing the mean inter-arrival time and the number of clients. Since GENI is highly unstable when number of nodes is large, nodes and connection time could be lost in several minutes when we use the 32-client topology, we reduce the number of clients to 16. The procedure of clients sending requests to servers and servers sending files to clients could be simplified to servers sending files to clients at certain intervals. This makes sense because requests from clients only generate small amount of network traffic and they should not be lost.  Since we are focusing on the throughput of the bottleneck link we do not change the bandwidth of the bottleneck link and almost no packet is lost during our measurement.  So for each set of parameters, we use D-itg to generate 16 flows for each client at both of the servers and start them at the same time. We use tcpdump to record all the traffic going through the bottleneck link at router G2. After 1000 seconds, the ITGSend flows end and we copy the tcpdump file to our computer. All the irrelevant packets are filtered out, and we use Matlab to generate the final figures. The inter-arrival time of packets are exponential distributed with mean rate of 5 packets per second. The packet sizes are Pareto distributed with alpha value exactly the same as the original paper (1.05, 1.35,1.95). As the original paper did not state the value of k for Pareto distribution, we set k to 200. For the exponential throughput measurement, we set the mean file size to 2000 bytes.

For the packet loss rate and buffer occupancy part, same 16-client topology is used. The value of k for Pareto distribution was not stated in the original paper, so by some trial and error we use k=200. Value of alpha is exactly the same as the original paper. The authors did not mention bandwidth of the bottleneck link for these measurements, so we set the bottleneck link to 1mbit/s. We set the queue buffer sizes exactly the same as the original paper using token bucket method. In addition, we added latency of 15ms to the links between the servers and routers. For each set of parameters, we run 5 replications of measurements each 100 seconds long. At the beginning of each replication, we first start tcpdump at router G2. Then start ITG with 16 flows to each of the clients at both servers. After about 5 seconds we start the queue size measurement script at router G2 and run it for 80 seconds. After the end of each replication we use Wireshark to analyze the tcpdump file and record packet loss rate and queue buffer occupancy to our data log.

## Step by step guidance for throughput measurement
1. Use the rspec file inluded here to reserve resources on GENI. The rspec already contains code to update the system and install d-itg. I used Rugters Instageni in my case.
2. Copy paras_fileTCPlong to both servers. Copy queuemon.sh to router G2. 
3. Run `sudo tc qdisc replace dev eth1 root netem delay 15ms` on the interface of S1 and S2 that are connected to G2.
4. Run tcpdump on G2's interface that is connected to G1. (`sudo tcpdump -i eth1 -w g2out.out` if it is eth1)
5. Run `ITGRecv` on all the client nodes.
6. Run `ITGSend paras_fileTCPlong -l server.log -x rcv.log` on both servers. Try to run them at almost the same time.
7. When ITGSend is finished, copy the tcpdump file to your computer and use Wireshark to analyze it. You could use `(ip.src==10.10.18.2||ip.src==10.10.19.2)` to filter out the packets sent by the servers if their ip addresses are 10.10.18.2 and 10.10.19.2.
8. Change the parameters in paras_fileTCPlong and run again.
9. To generate the plots, use Wireshark to get only the packets sent by servers and save the packets as csv files. Then load the csv files to Matlab. To load cvs files to Matlab, simply drag the cvs file to the command window, then select the file length and time variable and load them into Matlab as 2 variables. Then you can use code in NetworkFinalPlotTraffic.m to generate the figures.


## Step by step guidance for second set of experiments

1. Use the rspec file included here to reserve resources on GENI if you have not done so.
2. Copy paras_fileTCP2 to router G2. Copy queuemon.sh to G2. 
3. Run `sudo tc qdisc replace dev eth1 root netem delay 15ms` on the interface of S1 and S2 that are connected to G2.
4. Set bandwidth and buffer size for the bottleneck link. For G2's interface that is connected to G1,run 'sudo tc qdisc replace dev eth1 root tbf rate 1mbit limit 2kb burst 20kB'. The 2kB is the buffer size which would change for different runs.
5. Run tcpdump on G2's interface that is connected to G1.
6. Run `ITGRecv` on all the client nodes.
7. Run `ITGSend paras_fileTCP2 -l server.log -x rcv.log` on both servers. Try to run them at almost the same time.
8. After about 5 seconds when ITGSend has started, run `./queuemon.sh eth2 80 0.1 | tee router.txt` on G2.
9. After ITGSend is finished, run `cat router.txt | sed 's/\p / /g' | awk  '{ sum += $30 } END { if (NR > 0) print sum / NR }'`. The output is the queue buffer occupancy, write it down on your log file.
10. Copy the tcpdump file to your computer. Use wireshark to open it. You could use `tcp.analysis.lost_segment&&(ip.src==10.10.18.2||ip.src==10.10.19.2)` to get all the lost tcp packets. In the summary of the Statistics menu you can see how many bytes were lost, record this number. Then use `(ip.src==10.10.18.2||ip.src==10.10.19.2)` to find out how many bytes have been sent, record this number. Then you can compute the loss rate.
11. Run 5 replications for each set of parameters. Then change the parameters in the paras_fileTCP2 and change the link buffer and run again.
12. Compute average buffer occupancy and packet loss rate. Then use code in NetworkModelingFinalPlot.m to generate the figures.


##Note
1. I used GENI's default kernel for this experiment. The nodes are updated right after they were reserved.
2. I used Rugters Instageni for this experiment.
3. I used d-itg, wireshark and Matlab in this experiment.
4. The queuemonitor script was from our course's website (http://witestlab.poly.edu/~ffund/el7353/2-testbed-mm1.html)
5. The testbed does not support large number of nodes very well. So I had to scale down from 32 clients to 16 clients.
6. In the second set of experiment when alpha equals 1.05, the relative high volume of traffic may cause errors in D-ITG. So the actually sent traffic may be even lower than that of alpha=1.35. So please check the total amount of traffic and do not use data generated from the case of abnormally small amount of traffic.